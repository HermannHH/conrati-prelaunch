import React, { PureComponent } from 'react';
import styled from 'styled-components';
import store2 from 'store2';
import axios from 'axios';

import components from './components';

const BodyWrapper = styled.div`
    height:100%;
    height:100vh;
    width:100%;
    width:100vw;
    background-color: #163b6c;
    position: fixed;
    top: 0px;
    left: 0px;
`;

const Container = styled.div`
  display: table; 
  width:100%;
  height: 100%;
`;

const ContainedCenter = styled.div`
  display: table-cell; 
  vertical-align: middle;
`;

class App extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      isSubmitting: false,
      submitted: false,
    }
  }

  componentWillMount() {
    if (store2.get('submitted') ) {
      this.setState({
        submitted: true
      })
    }
  }

  handleChange(e) {
    console.log('e', e.target.id, e.target.value )
    this.setState({
      [e.target.id]: e.target.value
    })
  }
  
  handleSubmit(e) {
    e.preventDefault()
    this.setState({
      isSubmitting: true
    })
    const {
      email,
      name,
    } = this.state;
    axios.post('https://conrati-api-public-staging.stratech.co.za/contact_form', {
      api_token: '3c2152e7-1cae-40cf-b1b7-6364db324f0e',
      email,
      name
    }).then(
      (resp) => {
        console.log('resp', resp)
        this.setState({
          email: '',
          name: '',
          isSubmitting: false,
          submitted: true
        })
        store2.set('submitted', true)
      }
    ).catch((err) => {
      console.log('error', err)
    });
  }


  render() {
    const NotifyForm = components.NotifyForm;
    const {
      email,
      name,
      isSubmitting,
      submitted
    } = this.state;
    return (
      <BodyWrapper>
        <Container>
          <ContainedCenter>
            <NotifyForm
              email={email}
              name={name}
              isSubmitting={isSubmitting}
              submitted={submitted}
              handleChange={(e) => {this.handleChange(e)}}
              handleSubmit={(e) => {this.handleSubmit(e)}}

            />
          </ContainedCenter>
        </Container>
      </BodyWrapper>
    );
  }
}

export default App;
