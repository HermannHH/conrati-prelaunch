import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';

import ContactBanner from './ContactBanner';
import Logo from './Logo';
import Form from './Form';
import Added from './Added';


const Container = styled.div`
    background-color: #ffffff;
    border-radius: 7px;
    position: relative;
    display: block;
    margin: 0 auto;
    width: 30%;
    @media all and (max-width: 1199px) {
      width: 40%;
    };
    @media all and (max-width: 991px) {
      width: 50%;
    };
    @media all and (max-width: 768px) {
      width: 75%;
    };
    @media all and (max-width: 575px) {
      width: 90%;
    };
`;

const Divider = styled.div`
  border-bottom: thin solid #D4D4D4;
  width: 100%;
`;

function NotifyForm(props) {
  return (
    <Container>
      <Logo />
      <Divider />
      <ContactBanner />
      {props.submitted ?
        <Added {...props}/>
        :
        <div>
          <Divider />
          <Form {...props}/>
        </div>
      }
      
    </Container>
  );
};

// NotifyForm.propTypes = {
  
// };

export default NotifyForm;