import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';

const Container = styled.div`
    text-align: center;
    padding: 15px;
    background-color:  #FFB700;
    color: #ffffff;
    border-radius: 0px 0px 7px 7px;
`;


function Added(props) {
  return (
    <Container>
      <h3>Thank you for joining our waiting list</h3>
    </Container>
  );
};

// Added.propTypes = {
  
// };

export default Added;