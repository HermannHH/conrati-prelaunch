import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';

import ConratiLogo from '../../../conrati_logo.png';

const Container = styled.div`
    text-align: center;
    padding: 15px;
    > img {
      max-width: 100%;
      height: auto;
    }
`;

function Logo(props) {
  return (
    <Container>
      <img src={ConratiLogo} alt="conrati-logo" />
    </Container>
  );
};

// Logo.propTypes = {
  
// };

export default Logo;