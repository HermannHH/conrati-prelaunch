import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';

const Container = styled.div`
    text-align: center;
    padding: 15px;
    font-size: 13px;
    line-height: 0.8em;
    > .header {
      font-weight: 600;
      font-size: 15px;
      color: #0053A4;
    }
`;

function ContactBanner(props) {
  return (
    <Container>
      <p className="header">Website coming soon</p>
      <p>hennie@conrati.com | +49 151 2401 4801 </p>
      <p>Siemensstraße 2, 78532 Tuttlingen, Germany</p>
    </Container>
  );
};

// ContactBanner.propTypes = {
  
// };

export default ContactBanner;