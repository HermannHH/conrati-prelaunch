import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';

const Container = styled.div`
    text-align: center;
    padding: 15px;
    position: relative;
    > .header {
      font-weight: 600;
      font-size: 15px;
      color: #0053A4;
    };
`;

const InputWrapper = styled.div`
  padding: 15px;
  width: inherit;
  position: relative;
  > input {
    max-width: 100%;
    min-width: 100%;
    box-sizing: border-box;
    border: 1px solid #d4d4d4;
    border-radius: 9px;
    padding: 15px;
    font-size: 16px;
    outline: none;
  };
  > button {
    cursor: pointer;
    background-color: #FFB700;
    color: #ffffff;
    border: none;
    width: 100%;
    padding: 15px;
    border-radius: 20px;
    font-size: 16px;
    text-transform: uppercase;
    &:disabled{
      cursor: not-allowed;
      background-color: #eee;
      color: #a9a9a9;
    }
  }
`;


function validateEmail(email) {
  var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return re.test(email);
}

function isFormDisabled(props) {
  let isDisabled = true;
  if ( props.email !== '' && props.name !== '' && validateEmail(props.email) && props.isSubmitting === false ) {
    isDisabled = false;
  }
  return isDisabled
}

function Form(props) {
  return (
    <Container>
      <p className="header">Get notified of our site launch</p>
      <form onSubmit={props.handleSubmit}>
        <InputWrapper>
          <input
            autoComplete="off"
            type="text" 
            name="name" 
            id="name"
            placeholder="Your name"
            required
            onChange={props.handleChange}
            value={props.name}
          />
        </InputWrapper>
        <InputWrapper>
          <input
            autoComplete="off"
            type="email" 
            name="email" 
            id="email"
            placeholder="Email address"
            required
            onChange={props.handleChange}
            value={props.email}
          />
        </InputWrapper>
        <InputWrapper>
          <button 
            type="submit"
            disabled={isFormDisabled(props)}
          >
            {props.isSubmitting ?
              'Submitting...'
              :
              'Submit'
            }
          </button>
        </InputWrapper>
      </form>
    </Container>
  );
};

// Form.propTypes = {
  
// };

export default Form;